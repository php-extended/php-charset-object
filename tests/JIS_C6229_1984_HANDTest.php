<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JIS_C6229_1984_HAND;
use PHPUnit\Framework\TestCase;

/**
 * JIS_C6229_1984_HANDTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JIS_C6229_1984_HAND
 *
 * @internal
 *
 * @small
 */
class JIS_C6229_1984_HANDTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JIS_C6229_1984_HAND
	 */
	protected JIS_C6229_1984_HAND $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JIS_C6229_1984_HAND', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JIS_C6229-1984-hand', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JIS_C6229-1984-hand', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('70', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-94',
			'jp-ocr-hand',
			'csISO94JIS62291984hand',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JIS_C6229_1984_HAND();
	}
	
}
