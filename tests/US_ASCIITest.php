<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\US_ASCII;
use PHPUnit\Framework\TestCase;

/**
 * US_ASCIITest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\US_ASCII
 *
 * @internal
 *
 * @small
 */
class US_ASCIITest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var US_ASCII
	 */
	protected US_ASCII $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\US_ASCII', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('US-ASCII', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('US-ASCII', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('3', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('ANSI X3.4-1986', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(2046, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-6',
			'ANSI_X3.4-1968',
			'ANSI_X3.4-1986',
			'ISO_646.irv:1991',
			'ISO646-US',
			'US-ASCII',
			'us',
			'IBM367',
			'cp367',
			'csASCII',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new US_ASCII();
	}
	
}
