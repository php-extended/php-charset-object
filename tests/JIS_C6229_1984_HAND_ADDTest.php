<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JIS_C6229_1984_HAND_ADD;
use PHPUnit\Framework\TestCase;

/**
 * JIS_C6229_1984_HAND_ADDTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JIS_C6229_1984_HAND_ADD
 *
 * @internal
 *
 * @small
 */
class JIS_C6229_1984_HAND_ADDTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JIS_C6229_1984_HAND_ADD
	 */
	protected JIS_C6229_1984_HAND_ADD $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JIS_C6229_1984_HAND_ADD', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JIS_C6229-1984-hand-add', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JIS_C6229-1984-hand-add', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('71', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-95',
			'jp-ocr-hand-add',
			'csISO95JIS62291984handadd',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JIS_C6229_1984_HAND_ADD();
	}
	
}
