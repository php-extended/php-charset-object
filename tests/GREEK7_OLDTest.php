<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\GREEK7_OLD;
use PHPUnit\Framework\TestCase;

/**
 * GREEK7_OLDTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\GREEK7_OLD
 *
 * @internal
 *
 * @small
 */
class GREEK7_OLDTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GREEK7_OLD
	 */
	protected GREEK7_OLD $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\GREEK7_OLD', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('greek7-old', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('greek7-old', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('44', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-18',
			'csISO18Greek7Old',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GREEK7_OLD();
	}
	
}
