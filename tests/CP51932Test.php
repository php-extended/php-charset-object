<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CP51932;
use PHPUnit\Framework\TestCase;

/**
 * CP51932Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CP51932
 *
 * @internal
 *
 * @small
 */
class CP51932Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CP51932
	 */
	protected CP51932 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\CP51932', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('CP51932', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('CP51932', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2108', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/CP51932]                  [Yui_Naruse]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/CP51932]                  [Yui_Naruse', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csCP51932',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CP51932();
	}
	
}
