<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\HP_MATH8;
use PHPUnit\Framework\TestCase;

/**
 * HP_MATH8Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\HP_MATH8
 *
 * @internal
 *
 * @small
 */
class HP_MATH8Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HP_MATH8
	 */
	protected HP_MATH8 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\HP_MATH8', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('HP-Math8', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('HP-Math8', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2019', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PCL 5 Comparison Guide, Hewlett-Packard,
HP part number 5961-0510, October 1992
PCL Symbol Set id: 8M', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csHPMath8',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HP_MATH8();
	}
	
}
