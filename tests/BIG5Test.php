<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\BIG5;
use PHPUnit\Framework\TestCase;

/**
 * BIG5Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\BIG5
 *
 * @internal
 *
 * @small
 */
class BIG5Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BIG5
	 */
	protected BIG5 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\BIG5', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Big5', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Big5', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2026', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Chinese for Taiwan Multi-byte set.
PCL Symbol Set Id: 18T', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csBig5',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BIG5();
	}
	
}
