<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ECMA_CYRILLIC;
use PHPUnit\Framework\TestCase;

/**
 * ECMA_CYRILLICTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ECMA_CYRILLIC
 *
 * @internal
 *
 * @small
 */
class ECMA_CYRILLICTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ECMA_CYRILLIC
	 */
	protected ECMA_CYRILLIC $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ECMA_CYRILLIC', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ECMA-cyrillic', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ECMA-cyrillic', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('77', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO registry]
        (formerly [ECMA
          registry])', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-111',
			'KOI8-E',
			'csISO111ECMACyrillic',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ECMA_CYRILLIC();
	}
	
}
