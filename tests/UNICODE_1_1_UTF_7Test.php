<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\UNICODE_1_1_UTF_7;
use PHPUnit\Framework\TestCase;

/**
 * UNICODE_1_1_UTF_7Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\UNICODE_1_1_UTF_7
 *
 * @internal
 *
 * @small
 */
class UNICODE_1_1_UTF_7Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UNICODE_1_1_UTF_7
	 */
	protected UNICODE_1_1_UTF_7 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\UNICODE_1_1_UTF_7', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('UNICODE-1-1-UTF-7', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('UNICODE-1-1-UTF-7', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('103', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1642]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1642, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUnicode11UTF7',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UNICODE_1_1_UTF_7();
	}
	
}
