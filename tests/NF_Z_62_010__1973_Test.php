<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\NF_Z_62_010__1973_;
use PHPUnit\Framework\TestCase;

/**
 * NF_Z_62_010__1973_Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\NF_Z_62_010__1973_
 *
 * @internal
 *
 * @small
 */
class NF_Z_62_010__1973_Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NF_Z_62_010__1973_
	 */
	protected NF_Z_62_010__1973_ $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\NF_Z_62_010__1973_', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('NF_Z_62-010_(1973)', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('NF_Z_62-010_(1973)', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('46', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-25',
			'ISO646-FR1',
			'csISO25French',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NF_Z_62_010__1973_();
	}
	
}
