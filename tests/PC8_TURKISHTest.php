<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\PC8_TURKISH;
use PHPUnit\Framework\TestCase;

/**
 * PC8_TURKISHTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\PC8_TURKISH
 *
 * @internal
 *
 * @small
 */
class PC8_TURKISHTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PC8_TURKISH
	 */
	protected PC8_TURKISH $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\PC8_TURKISH', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('PC8-Turkish', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('PC8-Turkish', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2014', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PC Latin Turkish.  PCL Symbol Set id: 9T', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csPC8Turkish',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new PC8_TURKISH();
	}
	
}
