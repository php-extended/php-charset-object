<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\TSCII;
use PHPUnit\Framework\TestCase;

/**
 * TSCIITest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\TSCII
 *
 * @internal
 *
 * @small
 */
class TSCIITest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var TSCII
	 */
	protected TSCII $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\TSCII', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('TSCII', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('TSCII', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2107', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/TSCII]           [Kuppuswamy_Kalyanasu]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/TSCII]           [Kuppuswamy_Kalyanasu', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csTSCII',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new TSCII();
	}
	
}
