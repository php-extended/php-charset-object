<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM903;
use PHPUnit\Framework\TestCase;

/**
 * IBM903Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM903
 *
 * @internal
 *
 * @small
 */
class IBM903Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM903
	 */
	protected IBM903 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM903', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM903', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM903', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2059', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM NLS RM Vol2 SE09-8002-01, March 1990', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'cp903',
			'csIBM903',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM903();
	}
	
}
