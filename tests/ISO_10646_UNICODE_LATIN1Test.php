<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_10646_UNICODE_LATIN1;
use PHPUnit\Framework\TestCase;

/**
 * ISO_10646_UNICODE_LATIN1Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_10646_UNICODE_LATIN1
 *
 * @internal
 *
 * @small
 */
class ISO_10646_UNICODE_LATIN1Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_10646_UNICODE_LATIN1
	 */
	protected ISO_10646_UNICODE_LATIN1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_10646_UNICODE_LATIN1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-10646-Unicode-Latin1', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-10646-Unicode-Latin1', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1003', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('ISO Latin-1 subset of Unicode. Basic Latin and Latin-1
Supplement  = collections 1 and 2.  See ISO 10646,
Appendix A.  See [RFC1815].', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUnicodeLatin1',
			'ISO-10646',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_10646_UNICODE_LATIN1();
	}
	
}
