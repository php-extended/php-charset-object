<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IEC_P27_1;
use PHPUnit\Framework\TestCase;

/**
 * IEC_P27_1Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IEC_P27_1
 *
 * @internal
 *
 * @small
 */
class IEC_P27_1Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IEC_P27_1
	 */
	protected IEC_P27_1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IEC_P27_1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IEC_P27-1', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IEC_P27-1', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('88', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-143',
			'csISO143IECP271',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IEC_P27_1();
	}
	
}
