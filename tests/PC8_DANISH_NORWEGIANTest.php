<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\PC8_DANISH_NORWEGIAN;
use PHPUnit\Framework\TestCase;

/**
 * PC8_DANISH_NORWEGIANTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\PC8_DANISH_NORWEGIAN
 *
 * @internal
 *
 * @small
 */
class PC8_DANISH_NORWEGIANTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PC8_DANISH_NORWEGIAN
	 */
	protected PC8_DANISH_NORWEGIAN $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\PC8_DANISH_NORWEGIAN', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('PC8-Danish-Norwegian', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('PC8-Danish-Norwegian', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2012', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PC Danish Norwegian
8-bit PC set for Danish Norwegian
PCL Symbol Set id: 11U', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csPC8DanishNorwegian',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new PC8_DANISH_NORWEGIAN();
	}
	
}
