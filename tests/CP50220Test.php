<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CP50220;
use PHPUnit\Framework\TestCase;

/**
 * CP50220Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CP50220
 *
 * @internal
 *
 * @small
 */
class CP50220Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CP50220
	 */
	protected CP50220 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\CP50220', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('CP50220', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('CP50220', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2260', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/CP50220]                  [Yui_Naruse]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/CP50220]                  [Yui_Naruse', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csCP50220',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CP50220();
	}
	
}
