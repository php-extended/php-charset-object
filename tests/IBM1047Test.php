<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM1047;
use PHPUnit\Framework\TestCase;

/**
 * IBM1047Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM1047
 *
 * @internal
 *
 * @small
 */
class IBM1047Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM1047
	 */
	protected IBM1047 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM1047', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM1047', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM1047', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2102', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM1047 (EBCDIC Latin 1/Open Systems)
[http://www-1.ibm.com/servers/eserver/iseries/software/globalization/pdf/cp01047z.pdf]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www-1.ibm.com/servers/eserver/iseries/software/globalization/pdf/cp01047z.pdf', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'IBM-1047',
			'csIBM1047',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM1047();
	}
	
}
