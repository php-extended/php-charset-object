<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\TIS_620;
use PHPUnit\Framework\TestCase;

/**
 * TIS_620Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\TIS_620
 *
 * @internal
 *
 * @small
 */
class TIS_620Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var TIS_620
	 */
	protected TIS_620 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\TIS_620', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('TIS-620', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('TIS-620', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2259', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Thai Industrial Standards Institute (TISI)                             [Trin_Tantsetthi]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csTIS620',
			'ISO-8859-11',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new TIS_620();
	}
	
}
