<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_8859_8;
use PHPUnit\Framework\TestCase;

/**
 * ISO_8859_8Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_8859_8
 *
 * @internal
 *
 * @small
 */
class ISO_8859_8Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_8859_8
	 */
	protected ISO_8859_8 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_8859_8', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-8859-8', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO_8859-8:1988', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('11', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-138',
			'ISO_8859-8',
			'ISO-8859-8',
			'hebrew',
			'csISOLatinHebrew',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_8859_8();
	}
	
}
