<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_8859_6_E;
use PHPUnit\Framework\TestCase;

/**
 * ISO_8859_6_ETest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_8859_6_E
 *
 * @internal
 *
 * @small
 */
class ISO_8859_6_ETest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_8859_6_E
	 */
	protected ISO_8859_6_E $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_8859_6_E', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-8859-6-E', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO_8859-6-E', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('81', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1556]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1556, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csISO88596E',
			'ISO-8859-6-E',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_8859_6_E();
	}
	
}
