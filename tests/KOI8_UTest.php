<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\KOI8_U;
use PHPUnit\Framework\TestCase;

/**
 * KOI8_UTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\KOI8_U
 *
 * @internal
 *
 * @small
 */
class KOI8_UTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var KOI8_U
	 */
	protected KOI8_U $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\KOI8_U', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('KOI8-U', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('KOI8-U', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2088', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC2319]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(2319, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csKOI8U',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new KOI8_U();
	}
	
}
