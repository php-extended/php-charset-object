<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CESU_8;
use PHPUnit\Framework\TestCase;

/**
 * CESU_8Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CESU_8
 *
 * @internal
 *
 * @small
 */
class CESU_8Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CESU_8
	 */
	protected CESU_8 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\CESU_8', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('CESU-8', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('CESU-8', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1016', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[http://www.unicode.org/unicode/reports/tr26]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.unicode.org/unicode/reports/tr26', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csCESU8',
			'csCESU-8',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CESU_8();
	}
	
}
