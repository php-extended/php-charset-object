<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\MACINTOSH;
use PHPUnit\Framework\TestCase;

/**
 * MACINTOSHTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\MACINTOSH
 *
 * @internal
 *
 * @small
 */
class MACINTOSHTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MACINTOSH
	 */
	protected MACINTOSH $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\MACINTOSH', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('macintosh', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('macintosh', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2027', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('The Unicode Standard ver1.0, ISBN 0-201-56788-1, Oct 1991', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'mac',
			'csMacintosh',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MACINTOSH();
	}
	
}
