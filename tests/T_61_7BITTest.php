<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\T_61_7BIT;
use PHPUnit\Framework\TestCase;

/**
 * T_61_7BITTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\T_61_7BIT
 *
 * @internal
 *
 * @small
 */
class T_61_7BITTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var T_61_7BIT
	 */
	protected T_61_7BIT $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\T_61_7BIT', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('T.61-7bit', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('T.61-7bit', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('75', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-102',
			'csISO102T617bit',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new T_61_7BIT();
	}
	
}
