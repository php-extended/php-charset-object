<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JIS_X0212_1990;
use PHPUnit\Framework\TestCase;

/**
 * JIS_X0212_1990Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JIS_X0212_1990
 *
 * @internal
 *
 * @small
 */
class JIS_X0212_1990Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JIS_X0212_1990
	 */
	protected JIS_X0212_1990 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JIS_X0212_1990', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JIS_X0212-1990', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JIS_X0212-1990', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('98', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'x0212',
			'iso-ir-159',
			'csISO159JISX02121990',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JIS_X0212_1990();
	}
	
}
