<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\EBCDIC_UK;
use PHPUnit\Framework\TestCase;

/**
 * EBCDIC_UKTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\EBCDIC_UK
 *
 * @internal
 *
 * @small
 */
class EBCDIC_UKTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EBCDIC_UK
	 */
	protected EBCDIC_UK $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\EBCDIC_UK', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('EBCDIC-UK', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('EBCDIC-UK', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2077', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM 3270 Char Set Ref Ch 10, GA27-2837-9, April 1987', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csEBCDICUK',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EBCDIC_UK();
	}
	
}
