<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CSN_369103;
use PHPUnit\Framework\TestCase;

/**
 * CSN_369103Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CSN_369103
 *
 * @internal
 *
 * @small
 */
class CSN_369103Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CSN_369103
	 */
	protected CSN_369103 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\CSN_369103', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('CSN_369103', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('CSN_369103', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('86', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-139',
			'csISO139CSN369103',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CSN_369103();
	}
	
}
