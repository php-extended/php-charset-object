<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\NS_4551_2;
use PHPUnit\Framework\TestCase;

/**
 * NS_4551_2Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\NS_4551_2
 *
 * @internal
 *
 * @small
 */
class NS_4551_2Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NS_4551_2
	 */
	protected NS_4551_2 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\NS_4551_2', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('NS_4551-2', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('NS_4551-2', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('58', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'ISO646-NO2',
			'iso-ir-61',
			'no2',
			'csISO61Norwegian2',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NS_4551_2();
	}
	
}
