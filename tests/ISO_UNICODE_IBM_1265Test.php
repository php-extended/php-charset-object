<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_UNICODE_IBM_1265;
use PHPUnit\Framework\TestCase;

/**
 * ISO_UNICODE_IBM_1265Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_UNICODE_IBM_1265
 *
 * @internal
 *
 * @small
 */
class ISO_UNICODE_IBM_1265Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_UNICODE_IBM_1265
	 */
	protected ISO_UNICODE_IBM_1265 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_UNICODE_IBM_1265', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-Unicode-IBM-1265', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-Unicode-IBM-1265', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1009', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM Hebrew Presentation Set, GCSGID: 1265', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUnicodeIBM1265',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_UNICODE_IBM_1265();
	}
	
}
