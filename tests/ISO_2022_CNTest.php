<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_2022_CN;
use PHPUnit\Framework\TestCase;

/**
 * ISO_2022_CNTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_2022_CN
 *
 * @internal
 *
 * @small
 */
class ISO_2022_CNTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_2022_CN
	 */
	protected ISO_2022_CN $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_2022_CN', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-2022-CN', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-2022-CN', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('104', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1922]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1922, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csISO2022CN',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_2022_CN();
	}
	
}
