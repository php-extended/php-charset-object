<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_10367_BOX;
use PHPUnit\Framework\TestCase;

/**
 * ISO_10367_BOXTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_10367_BOX
 *
 * @internal
 *
 * @small
 */
class ISO_10367_BOXTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_10367_BOX
	 */
	protected ISO_10367_BOX $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_10367_BOX', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO_10367-box', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO_10367-box', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('96', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-155',
			'csISO10367Box',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_10367_BOX();
	}
	
}
