<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_10646_UCS_4;
use PHPUnit\Framework\TestCase;

/**
 * ISO_10646_UCS_4Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_10646_UCS_4
 *
 * @internal
 *
 * @small
 */
class ISO_10646_UCS_4Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_10646_UCS_4
	 */
	protected ISO_10646_UCS_4 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_10646_UCS_4', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-10646-UCS-4', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-10646-UCS-4', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1001', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('the full code space. (same comment about byte order,
these are 31-bit numbers.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUCS4',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_10646_UCS_4();
	}
	
}
