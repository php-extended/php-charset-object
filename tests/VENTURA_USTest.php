<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\VENTURA_US;
use PHPUnit\Framework\TestCase;

/**
 * VENTURA_USTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\VENTURA_US
 *
 * @internal
 *
 * @small
 */
class VENTURA_USTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var VENTURA_US
	 */
	protected VENTURA_US $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\VENTURA_US', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Ventura-US', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Ventura-US', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2006', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Ventura US.  ASCII plus characters typically used in
publishing, like pilcrow, copyright, registered, trade mark,
section, dagger, and double dagger in the range A0 (hex)
to FF (hex).
PCL Symbol Set id: 14J', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csVenturaUS',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new VENTURA_US();
	}
	
}
