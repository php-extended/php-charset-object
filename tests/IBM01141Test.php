<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM01141;
use PHPUnit\Framework\TestCase;

/**
 * IBM01141Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM01141
 *
 * @internal
 *
 * @small
 */
class IBM01141Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM01141
	 */
	protected IBM01141 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM01141', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM01141', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM01141', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2092', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM See [http://www.iana.org/assignments/charset-reg/IBM01141]    [Tamer_Mahdi]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/IBM01141]    [Tamer_Mahdi', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'CCSID01141',
			'CP01141',
			'ebcdic-de-273+euro',
			'csIBM01141',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM01141();
	}
	
}
