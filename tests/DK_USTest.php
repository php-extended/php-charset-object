<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\DK_US;
use PHPUnit\Framework\TestCase;

/**
 * DK_USTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\DK_US
 *
 * @internal
 *
 * @small
 */
class DK_USTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DK_US
	 */
	protected DK_US $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\DK_US', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('dk-us', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('dk-us', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('101', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertNull($this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csDKUS',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new DK_US();
	}
	
}
