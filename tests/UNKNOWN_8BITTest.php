<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\UNKNOWN_8BIT;
use PHPUnit\Framework\TestCase;

/**
 * UNKNOWN_8BITTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\UNKNOWN_8BIT
 *
 * @internal
 *
 * @small
 */
class UNKNOWN_8BITTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UNKNOWN_8BIT
	 */
	protected UNKNOWN_8BIT $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\UNKNOWN_8BIT', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('UNKNOWN-8BIT', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('UNKNOWN-8BIT', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2079', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertNull($this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1428, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUnknown8BiT',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UNKNOWN_8BIT();
	}
	
}
