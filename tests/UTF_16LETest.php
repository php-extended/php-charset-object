<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\UTF_16LE;
use PHPUnit\Framework\TestCase;

/**
 * UTF_16LETest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\UTF_16LE
 *
 * @internal
 *
 * @small
 */
class UTF_16LETest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UTF_16LE
	 */
	protected UTF_16LE $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\UTF_16LE', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('UTF-16LE', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('UTF-16LE', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1014', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC2781]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(2781, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUTF16LE',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UTF_16LE();
	}
	
}
