<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_11548_1;
use PHPUnit\Framework\TestCase;

/**
 * ISO_11548_1Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_11548_1
 *
 * @internal
 *
 * @small
 */
class ISO_11548_1Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_11548_1
	 */
	protected ISO_11548_1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_11548_1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-11548-1', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-11548-1', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('118', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/ISO-11548-1]            [Samuel_Thibault]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/ISO-11548-1]            [Samuel_Thibault', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'ISO_11548-1',
			'ISO_TR_11548-1',
			'csISO115481',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_11548_1();
	}
	
}
