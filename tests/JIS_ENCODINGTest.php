<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JIS_ENCODING;
use PHPUnit\Framework\TestCase;

/**
 * JIS_ENCODINGTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JIS_ENCODING
 *
 * @internal
 *
 * @small
 */
class JIS_ENCODINGTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JIS_ENCODING
	 */
	protected JIS_ENCODING $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JIS_ENCODING', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JIS_Encoding', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JIS_Encoding', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('16', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('JIS X 0202-1991.  Uses ISO 2022 escape sequences to
shift code sets as documented in JIS X 0202-1991.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csJISEncoding',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JIS_ENCODING();
	}
	
}
