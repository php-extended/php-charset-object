<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_8859_SUPP;
use PHPUnit\Framework\TestCase;

/**
 * ISO_8859_SUPPTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_8859_SUPP
 *
 * @internal
 *
 * @small
 */
class ISO_8859_SUPPTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_8859_SUPP
	 */
	protected ISO_8859_SUPP $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_8859_SUPP', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO_8859-supp', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO_8859-supp', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('95', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-154',
			'latin1-2-5',
			'csISO8859Supp',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_8859_SUPP();
	}
	
}
