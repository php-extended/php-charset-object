<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\EUC_KR;
use PHPUnit\Framework\TestCase;

/**
 * EUC_KRTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\EUC_KR
 *
 * @internal
 *
 * @small
 */
class EUC_KRTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EUC_KR
	 */
	protected EUC_KR $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\EUC_KR', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('EUC-KR', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('EUC-KR', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('38', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1557] (see also KS_C_5861-1992)', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1557, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csEUCKR',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EUC_KR();
	}
	
}
