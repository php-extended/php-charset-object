<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\BIG5_HKSCS;
use PHPUnit\Framework\TestCase;

/**
 * BIG5_HKSCSTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\BIG5_HKSCS
 *
 * @internal
 *
 * @small
 */
class BIG5_HKSCSTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BIG5_HKSCS
	 */
	protected BIG5_HKSCS $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\BIG5_HKSCS', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Big5-HKSCS', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Big5-HKSCS', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2101', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/Big5-HKSCS]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/Big5-HKSCS', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csBig5HKSCS',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BIG5_HKSCS();
	}
	
}
