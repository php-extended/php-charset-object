<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\VENTURA_MATH;
use PHPUnit\Framework\TestCase;

/**
 * VENTURA_MATHTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\VENTURA_MATH
 *
 * @internal
 *
 * @small
 */
class VENTURA_MATHTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var VENTURA_MATH
	 */
	protected VENTURA_MATH $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\VENTURA_MATH', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Ventura-Math', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Ventura-Math', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2022', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PCL 5 Comparison Guide, Hewlett-Packard,
HP part number 5961-0510, October 1992
PCL Symbol Set id: 6M', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csVenturaMath',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new VENTURA_MATH();
	}
	
}
