<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\DS_2089;
use PHPUnit\Framework\TestCase;

/**
 * DS_2089Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\DS_2089
 *
 * @internal
 *
 * @small
 */
class DS_2089Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DS_2089
	 */
	protected DS_2089 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\DS_2089', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('DS_2089', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('DS_2089', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('99', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Danish Standard, DS 2089, February 1974', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'DS2089',
			'ISO646-DK',
			'dk',
			'csISO646Danish',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new DS_2089();
	}
	
}
