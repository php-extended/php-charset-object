<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM_SYMBOLS;
use PHPUnit\Framework\TestCase;

/**
 * IBM_SYMBOLSTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM_SYMBOLS
 *
 * @internal
 *
 * @small
 */
class IBM_SYMBOLSTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM_SYMBOLS
	 */
	protected IBM_SYMBOLS $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM_SYMBOLS', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM-Symbols', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM-Symbols', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2015', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Presentation Set, CPGID: 259', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csIBMSymbols',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM_SYMBOLS();
	}
	
}
