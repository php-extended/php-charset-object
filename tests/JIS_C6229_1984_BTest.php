<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JIS_C6229_1984_B;
use PHPUnit\Framework\TestCase;

/**
 * JIS_C6229_1984_BTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JIS_C6229_1984_B
 *
 * @internal
 *
 * @small
 */
class JIS_C6229_1984_BTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JIS_C6229_1984_B
	 */
	protected JIS_C6229_1984_B $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JIS_C6229_1984_B', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JIS_C6229-1984-b', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JIS_C6229-1984-b', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('68', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-92',
			'ISO646-JP-OCR-B',
			'jp-ocr-b',
			'csISO92JISC62991984b',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JIS_C6229_1984_B();
	}
	
}
