<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\VIDEOTEX_SUPPL;
use PHPUnit\Framework\TestCase;

/**
 * VIDEOTEX_SUPPLTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\VIDEOTEX_SUPPL
 *
 * @internal
 *
 * @small
 */
class VIDEOTEX_SUPPLTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var VIDEOTEX_SUPPL
	 */
	protected VIDEOTEX_SUPPL $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\VIDEOTEX_SUPPL', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('videotex-suppl', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('videotex-suppl', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('59', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-70',
			'csISO70VideotexSupp1',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new VIDEOTEX_SUPPL();
	}
	
}
