<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\GB2312;
use PHPUnit\Framework\TestCase;

/**
 * GB2312Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\GB2312
 *
 * @internal
 *
 * @small
 */
class GB2312Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GB2312
	 */
	protected GB2312 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\GB2312', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('GB2312', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('GB2312', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2025', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Chinese for People\'s Republic of China (PRC) one byte,
two byte set:
20-7E = one byte ASCII
A1-FE = two byte PRC Kanji
See GB 2312-80
PCL Symbol Set Id: 18C', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csGB2312',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GB2312();
	}
	
}
