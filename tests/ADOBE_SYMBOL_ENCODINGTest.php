<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ADOBE_SYMBOL_ENCODING;
use PHPUnit\Framework\TestCase;

/**
 * ADOBE_SYMBOL_ENCODINGTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ADOBE_SYMBOL_ENCODING
 *
 * @internal
 *
 * @small
 */
class ADOBE_SYMBOL_ENCODINGTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ADOBE_SYMBOL_ENCODING
	 */
	protected ADOBE_SYMBOL_ENCODING $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ADOBE_SYMBOL_ENCODING', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Adobe-Symbol-Encoding', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Adobe-Symbol-Encoding', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2020', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PostScript Language Reference Manual
PCL Symbol Set id: 5M', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csHPPSMath',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ADOBE_SYMBOL_ENCODING();
	}
	
}
