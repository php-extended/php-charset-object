<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\GOST_19768_74;
use PHPUnit\Framework\TestCase;

/**
 * GOST_19768_74Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\GOST_19768_74
 *
 * @internal
 *
 * @small
 */
class GOST_19768_74Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GOST_19768_74
	 */
	protected GOST_19768_74 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\GOST_19768_74', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('GOST_19768-74', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('GOST_19768-74', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('94', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'ST_SEV_358-88',
			'iso-ir-153',
			'csISO153GOST1976874',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GOST_19768_74();
	}
	
}
