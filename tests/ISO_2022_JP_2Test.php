<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_2022_JP_2;
use PHPUnit\Framework\TestCase;

/**
 * ISO_2022_JP_2Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_2022_JP_2
 *
 * @internal
 *
 * @small
 */
class ISO_2022_JP_2Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_2022_JP_2
	 */
	protected ISO_2022_JP_2 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_2022_JP_2', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-2022-JP-2', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-2022-JP-2', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('40', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1554]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1554, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csISO2022JP2',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_2022_JP_2();
	}
	
}
