<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\MICROSOFT_PUBLISHING;
use PHPUnit\Framework\TestCase;

/**
 * MICROSOFT_PUBLISHINGTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\MICROSOFT_PUBLISHING
 *
 * @internal
 *
 * @small
 */
class MICROSOFT_PUBLISHINGTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MICROSOFT_PUBLISHING
	 */
	protected MICROSOFT_PUBLISHING $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\MICROSOFT_PUBLISHING', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Microsoft-Publishing', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Microsoft-Publishing', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2023', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PCL 5 Comparison Guide, Hewlett-Packard,
HP part number 5961-0510, October 1992
PCL Symbol Set id: 6J', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csMicrosoftPublishing',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MICROSOFT_PUBLISHING();
	}
	
}
