<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\VENTURA_INTERNATIONAL;
use PHPUnit\Framework\TestCase;

/**
 * VENTURA_INTERNATIONALTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\VENTURA_INTERNATIONAL
 *
 * @internal
 *
 * @small
 */
class VENTURA_INTERNATIONALTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var VENTURA_INTERNATIONAL
	 */
	protected VENTURA_INTERNATIONAL $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\VENTURA_INTERNATIONAL', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Ventura-International', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Ventura-International', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2007', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Ventura International.  ASCII plus coded characters similar
to Roman8.
PCL Symbol Set id: 13J', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csVenturaInternational',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new VENTURA_INTERNATIONAL();
	}
	
}
