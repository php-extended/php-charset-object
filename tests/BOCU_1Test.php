<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\BOCU_1;
use PHPUnit\Framework\TestCase;

/**
 * BOCU_1Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\BOCU_1
 *
 * @internal
 *
 * @small
 */
class BOCU_1Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BOCU_1
	 */
	protected BOCU_1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\BOCU_1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('BOCU-1', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('BOCU-1', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1020', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[http://www.unicode.org/notes/tn6/]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.unicode.org/notes/tn6/', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csBOCU1',
			'csBOCU-1',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BOCU_1();
	}
	
}
