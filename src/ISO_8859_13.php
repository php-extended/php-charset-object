<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Charset;

/**
 * ISO_8859_13 class file.
 * 
 * This class contains the metadata about the ISO-8859-13 character set.
 * 
 * /!\ Do not edit this file by hand, as it is written automatically. /!\
 * See PhpExtended\Charset\CharacterSetGenerator for more details.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortClassName")
 */
class ISO_8859_13 implements CharacterSetInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return __CLASS__;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getName()
	 */
	public function getName() : string
	{
		return 'ISO-8859-13';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getFullName()
	 */
	public function getFullName() : string
	{
		return 'ISO-8859-13';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getMIBenum()
	 */
	public function getMIBenum() : int
	{
		return 109;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getSourceComment()
	 */
	public function getSourceComment() : ?string
	{
		return 'ISO See [http://www.iana.org/assignments/charset-reg/ISO-8859-13][Vladas_Tumasonis]';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getSourceUrl()
	 */
	public function getSourceUrl() : ?string
	{
		return 'http://www.iana.org/assignments/charset-reg/ISO-8859-13][Vladas_Tumasonis';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getRFCNumber()
	 */
	public function getRFCNumber() : ?int
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getAliases()
	 */
	public function getAliases() : array
	{
		return [
			'csISO885913',
		];
	}
	
}
