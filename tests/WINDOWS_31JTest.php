<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\WINDOWS_31J;
use PHPUnit\Framework\TestCase;

/**
 * WINDOWS_31JTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\WINDOWS_31J
 *
 * @internal
 *
 * @small
 */
class WINDOWS_31JTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var WINDOWS_31J
	 */
	protected WINDOWS_31J $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\WINDOWS_31J', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Windows-31J', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Windows-31J', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2024', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Windows Japanese.  A further extension of Shift_JIS
to include NEC special characters (Row 13), NEC
selection of IBM extensions (Rows 89 to 92), and IBM
extensions (Rows 115 to 119).  The CCS\'s are
JIS X0201:1997, JIS X0208:1997, and these extensions.
This charset can be used for the top-level media type "text",
but it is of limited or specialized use (see [RFC2278]).
PCL Symbol Set id: 19K', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csWindows31J',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new WINDOWS_31J();
	}
	
}
