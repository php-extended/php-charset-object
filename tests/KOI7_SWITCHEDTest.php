<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\KOI7_SWITCHED;
use PHPUnit\Framework\TestCase;

/**
 * KOI7_SWITCHEDTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\KOI7_SWITCHED
 *
 * @internal
 *
 * @small
 */
class KOI7_SWITCHEDTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var KOI7_SWITCHED
	 */
	protected KOI7_SWITCHED $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\KOI7_SWITCHED', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('KOI7-switched', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('KOI7-switched', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2105', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/KOI7-switched]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/KOI7-switched', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csKOI7switched',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new KOI7_SWITCHED();
	}
	
}
