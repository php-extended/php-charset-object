<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Charset;

/**
 * CharacterSetReference class file.
 * 
 * This class is a simple implementation of the CharacterSetReferenceInterface.
 * 
 * @author Anastaszor
 */
class CharacterSetReference implements CharacterSetReferenceInterface
{
	
	/**
	 * The map of ids to charset classnames.
	 * 
	 * @var array<string, class-string>
	 */
	protected $_idMap = [];
	
	/**
	 * The map of aliases to charset classnames.
	 * 
	 * @var array<string, class-string>
	 */
	protected $_aliasMap = [];
	
	/**
	 * The map of classes that exist.
	 * 
	 * @var array<class-string, integer>
	 */
	protected $_classMap = [];
	
	/**
	 * The instances of character set that are already.
	 * 
	 * @var array<class-string, CharacterSetInterface>
	 */
	protected $_objectMap = [];
	
	/**
	 * Populates this references with the external dictionnaries.
	 */
	public function __construct()
	{
		$this->_idMap = require \dirname(__DIR__).'/data/id_map.php';
		$this->_aliasMap = require \dirname(__DIR__).'/data/alias_map.php';
		$this->_classMap = require \dirname(__DIR__).'/data/class_map.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetReferenceInterface::lookup()
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress LessSpecificReturnStatement
	 */
	public function lookup(string $name) : ?CharacterSetInterface
	{
		if(false !== \mb_strpos($name, '\\'))
		{
			if(isset($this->_classMap[$name]))
			{
				if(isset($this->_objectMap[$name]))
				{
					return $this->_objectMap[$name];
				}
				
				/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
				return $this->_objectMap[$name] = new $name();
			}
		}
		$lowerName = (string) \mb_strtolower($name);
		if(isset($this->_idMap[$lowerName]))
		{
			$className = $this->_idMap[$lowerName];
			if(isset($this->_objectMap[$className]))
			{
				return $this->_objectMap[$className];
			}
			
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion,MixedMethodCall */
			return $this->_objectMap[$className] = new $className();
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetReferenceInterface::lookupAlias()
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress LessSpecificReturnStatement
	 */
	public function lookupAlias(string $alias) : ?CharacterSetInterface
	{
		$lowerName = (string) \mb_strtolower($alias);
		$lookup = $this->lookup($lowerName);
		if(null !== $lookup)
		{
		return $lookup;
		}
		if(isset($this->_aliasMap[$lowerName]))
		{
			$className = $this->_aliasMap[$lowerName];
			if(isset($this->_objectMap[$className]))
			{
				return $this->_objectMap[$className];
			}
			
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion,MixedMethodCall */
			return $this->_objectMap[$className] = new $className();
		}
		
		return null;
	}
	
}
