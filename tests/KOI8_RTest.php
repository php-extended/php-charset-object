<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\KOI8_R;
use PHPUnit\Framework\TestCase;

/**
 * KOI8_RTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\KOI8_R
 *
 * @internal
 *
 * @small
 */
class KOI8_RTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var KOI8_R
	 */
	protected KOI8_R $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\KOI8_R', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('KOI8-R', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('KOI8-R', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2084', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1489], based on GOST-19768-74, ISO-6937/8,
INIS-Cyrillic, ISO-5427.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1489, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csKOI8R',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new KOI8_R();
	}
	
}
