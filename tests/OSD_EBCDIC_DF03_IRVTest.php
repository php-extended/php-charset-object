<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\OSD_EBCDIC_DF03_IRV;
use PHPUnit\Framework\TestCase;

/**
 * OSD_EBCDIC_DF03_IRVTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\OSD_EBCDIC_DF03_IRV
 *
 * @internal
 *
 * @small
 */
class OSD_EBCDIC_DF03_IRVTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OSD_EBCDIC_DF03_IRV
	 */
	protected OSD_EBCDIC_DF03_IRV $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\OSD_EBCDIC_DF03_IRV', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('OSD_EBCDIC_DF03_IRV', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('OSD_EBCDIC_DF03_IRV', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('116', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Fujitsu-Siemens standard mainframe EBCDIC encoding
Please see: [http://www.iana.org/assignments/charset-reg/OSD-EBCDIC-DF03-IRV]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/OSD-EBCDIC-DF03-IRV', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csOSDEBCDICDF03IRV',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OSD_EBCDIC_DF03_IRV();
	}
	
}
