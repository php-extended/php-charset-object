<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\SCSU;
use PHPUnit\Framework\TestCase;

/**
 * SCSUTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\SCSU
 *
 * @internal
 *
 * @small
 */
class SCSUTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var SCSU
	 */
	protected SCSU $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\SCSU', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('SCSU', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('SCSU', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1011', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('SCSU See [http://www.iana.org/assignments/charset-reg/SCSU]     [Markus_Scherer]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/SCSU]     [Markus_Scherer', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csSCSU',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new SCSU();
	}
	
}
