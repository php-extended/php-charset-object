<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\GB18030;
use PHPUnit\Framework\TestCase;

/**
 * GB18030Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\GB18030
 *
 * @internal
 *
 * @small
 */
class GB18030Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GB18030
	 */
	protected GB18030 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\GB18030', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('GB18030', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('GB18030', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('114', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Chinese IT Standardization Technical Committee
Please see: [http://www.iana.org/assignments/charset-reg/GB18030]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/GB18030', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csGB18030',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GB18030();
	}
	
}
