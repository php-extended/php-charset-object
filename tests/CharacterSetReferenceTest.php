<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CharacterSetReference;
use PHPUnit\Framework\TestCase;

/**
 * CharacterSetReferenceTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CharacterSetReference
 *
 * @internal
 *
 * @small
 */
class CharacterSetReferenceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CharacterSetReference
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CharacterSetReference();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
