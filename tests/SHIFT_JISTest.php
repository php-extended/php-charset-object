<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\SHIFT_JIS;
use PHPUnit\Framework\TestCase;

/**
 * SHIFT_JISTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\SHIFT_JIS
 *
 * @internal
 *
 * @small
 */
class SHIFT_JISTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var SHIFT_JIS
	 */
	protected SHIFT_JIS $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\SHIFT_JIS', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Shift_JIS', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Shift_JIS', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('17', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('This charset is an extension of csHalfWidthKatakana by
adding graphic characters in JIS X 0208.  The CCS\'s are
JIS X0201:1997 and JIS X0208:1997.  The
complete definition is shown in Appendix 1 of JIS
X0208:1997.
This charset can be used for the top-level media type "text".', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'MS_Kanji',
			'csShiftJIS',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new SHIFT_JIS();
	}
	
}
