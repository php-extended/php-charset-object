<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_8859_9_WINDOWS_LATIN_5;
use PHPUnit\Framework\TestCase;

/**
 * ISO_8859_9_WINDOWS_LATIN_5Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_8859_9_WINDOWS_LATIN_5
 *
 * @internal
 *
 * @small
 */
class ISO_8859_9_WINDOWS_LATIN_5Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_8859_9_WINDOWS_LATIN_5
	 */
	protected ISO_8859_9_WINDOWS_LATIN_5 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_8859_9_WINDOWS_LATIN_5', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-8859-9-Windows-Latin-5', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-8859-9-Windows-Latin-5', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2003', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Extended ISO 8859-9.  Latin-5 for Windows 3.1
PCL Symbol Set id: 5T', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csWindows31Latin5',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_8859_9_WINDOWS_LATIN_5();
	}
	
}
