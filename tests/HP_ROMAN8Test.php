<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\HP_ROMAN8;
use PHPUnit\Framework\TestCase;

/**
 * HP_ROMAN8Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\HP_ROMAN8
 *
 * @internal
 *
 * @small
 */
class HP_ROMAN8Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HP_ROMAN8
	 */
	protected HP_ROMAN8 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\HP_ROMAN8', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('hp-roman8', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('hp-roman8', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2004', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('LaserJet IIP Printer User\'s Manual,
HP part no 33471-90901, Hewlet-Packard, June 1989.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'roman8',
			'r8',
			'csHPRoman8',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HP_ROMAN8();
	}
	
}
