<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\VISCII;
use PHPUnit\Framework\TestCase;

/**
 * VISCIITest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\VISCII
 *
 * @internal
 *
 * @small
 */
class VISCIITest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var VISCII
	 */
	protected VISCII $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\VISCII', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('VISCII', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('VISCII', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2082', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1456]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1456, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csVISCII',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new VISCII();
	}
	
}
