<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CSA_Z243_4_1985_2;
use PHPUnit\Framework\TestCase;

/**
 * CSA_Z243_4_1985_2Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CSA_Z243_4_1985_2
 *
 * @internal
 *
 * @small
 */
class CSA_Z243_4_1985_2Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CSA_Z243_4_1985_2
	 */
	protected CSA_Z243_4_1985_2 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\CSA_Z243_4_1985_2', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('CSA_Z243.4-1985-2', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('CSA_Z243.4-1985-2', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('79', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-122',
			'ISO646-CA2',
			'csa7-2',
			'csa72',
			'csISO122Canadian2',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CSA_Z243_4_1985_2();
	}
	
}
