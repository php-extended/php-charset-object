<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\OSD_EBCDIC_DF04_15;
use PHPUnit\Framework\TestCase;

/**
 * OSD_EBCDIC_DF04_15Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\OSD_EBCDIC_DF04_15
 *
 * @internal
 *
 * @small
 */
class OSD_EBCDIC_DF04_15Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OSD_EBCDIC_DF04_15
	 */
	protected OSD_EBCDIC_DF04_15 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\OSD_EBCDIC_DF04_15', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('OSD_EBCDIC_DF04_15', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('OSD_EBCDIC_DF04_15', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('115', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Fujitsu-Siemens standard mainframe EBCDIC encoding
Please see: [http://www.iana.org/assignments/charset-reg/OSD-EBCDIC-DF04-15]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/OSD-EBCDIC-DF04-15', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csOSDEBCDICDF0415',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OSD_EBCDIC_DF04_15();
	}
	
}
