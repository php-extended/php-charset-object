<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\HP_PI_FONT;
use PHPUnit\Framework\TestCase;

/**
 * HP_PI_FONTTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\HP_PI_FONT
 *
 * @internal
 *
 * @small
 */
class HP_PI_FONTTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HP_PI_FONT
	 */
	protected HP_PI_FONT $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\HP_PI_FONT', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('HP-Pi-font', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('HP-Pi-font', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2018', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PCL 5 Comparison Guide, Hewlett-Packard,
HP part number 5961-0510, October 1992
PCL Symbol Set id: 15U', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csHPPiFont',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HP_PI_FONT();
	}
	
}
