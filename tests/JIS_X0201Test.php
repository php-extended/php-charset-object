<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JIS_X0201;
use PHPUnit\Framework\TestCase;

/**
 * JIS_X0201Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JIS_X0201
 *
 * @internal
 *
 * @small
 */
class JIS_X0201Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JIS_X0201
	 */
	protected JIS_X0201 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JIS_X0201', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JIS_X0201', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JIS_X0201', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('15', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('JIS X 0201-1976.   One byte only, this is equivalent to
JIS/Roman (similar to ASCII) plus eight-bit half-width
Katakana', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'X0201',
			'csHalfWidthKatakana',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JIS_X0201();
	}
	
}
