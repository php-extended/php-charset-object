<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM865;
use PHPUnit\Framework\TestCase;

/**
 * IBM865Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM865
 *
 * @internal
 *
 * @small
 */
class IBM865Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM865
	 */
	protected IBM865 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM865', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM865', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM865', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2052', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM DOS 3.3 Ref (Abridged), 94X9575 (Feb 1987)', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'cp865',
			'865',
			'csIBM865',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM865();
	}
	
}
