<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\EUC_JP;
use PHPUnit\Framework\TestCase;

/**
 * EUC_JPTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\EUC_JP
 *
 * @internal
 *
 * @small
 */
class EUC_JPTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EUC_JP
	 */
	protected EUC_JP $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\EUC_JP', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('EUC-JP', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Extended_UNIX_Code_Packed_Format_for_Japanese', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('18', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Standardized by OSF, UNIX International, and UNIX Systems
Laboratories Pacific.  Uses ISO 2022 rules to select
code set 0: US-ASCII (a single 7-bit byte set)
code set 1: JIS X0208-1990 (a double 8-bit byte set)
restricted to A0-FF in both bytes
code set 2: Half Width Katakana (a single 7-bit byte set)
requiring SS2 as the character prefix
code set 3: JIS X0212-1990 (a double 7-bit byte set)
restricted to A0-FF in both bytes
requiring SS3 as the character prefix', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csEUCPkdFmtJapanese',
			'EUC-JP',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EUC_JP();
	}
	
}
