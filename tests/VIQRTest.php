<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\VIQR;
use PHPUnit\Framework\TestCase;

/**
 * VIQRTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\VIQR
 *
 * @internal
 *
 * @small
 */
class VIQRTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var VIQR
	 */
	protected VIQR $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\VIQR', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('VIQR', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('VIQR', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2083', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1456]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1456, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csVIQR',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new VIQR();
	}
	
}
