<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\MNEMONIC;
use PHPUnit\Framework\TestCase;

/**
 * MNEMONICTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\MNEMONIC
 *
 * @internal
 *
 * @small
 */
class MNEMONICTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MNEMONIC
	 */
	protected MNEMONIC $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\MNEMONIC', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('MNEMONIC', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('MNEMONIC', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2080', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[RFC1345], also known as "mnemonic+ascii+38"', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csMnemonic',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MNEMONIC();
	}
	
}
