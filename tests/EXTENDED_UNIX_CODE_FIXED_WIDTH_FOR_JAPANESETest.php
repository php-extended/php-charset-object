<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE;
use PHPUnit\Framework\TestCase;

/**
 * EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESETest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE
 *
 * @internal
 *
 * @small
 */
class EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESETest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE
	 */
	protected EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Extended_UNIX_Code_Fixed_Width_for_Japanese', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Extended_UNIX_Code_Fixed_Width_for_Japanese', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('19', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Used in Japan.  Each character is 2 octets.
code set 0: US-ASCII (a single 7-bit byte set)
1st byte = 00
2nd byte = 20-7E
code set 1: JIS X0208-1990 (a double 7-bit byte set)
restricted  to A0-FF in both bytes
code set 2: Half Width Katakana (a single 7-bit byte set)
1st byte = 00
2nd byte = A0-FF
code set 3: JIS X0212-1990 (a double 7-bit byte set)
restricted to A0-FF in
the first byte
and 21-7E in the second byte', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csEUCFixWidJapanese',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE();
	}
	
}
