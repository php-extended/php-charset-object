<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\DEC_MCS;
use PHPUnit\Framework\TestCase;

/**
 * DEC_MCSTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\DEC_MCS
 *
 * @internal
 *
 * @small
 */
class DEC_MCSTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DEC_MCS
	 */
	protected DEC_MCS $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\DEC_MCS', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('DEC-MCS', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('DEC-MCS', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2008', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('VAX/VMS User\'s Manual,
Order Number: AI-Y517A-TE, April 1986.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'dec',
			'csDECMCS',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new DEC_MCS();
	}
	
}
