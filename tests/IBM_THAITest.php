<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM_THAI;
use PHPUnit\Framework\TestCase;

/**
 * IBM_THAITest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM_THAI
 *
 * @internal
 *
 * @small
 */
class IBM_THAITest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM_THAI
	 */
	protected IBM_THAI $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM_THAI', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM-Thai', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM-Thai', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2016', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Presentation Set, CPGID: 838', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csIBMThai',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM_THAI();
	}
	
}
