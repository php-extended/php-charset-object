<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\WINDOWS_1255;
use PHPUnit\Framework\TestCase;

/**
 * WINDOWS_1255Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\WINDOWS_1255
 *
 * @internal
 *
 * @small
 */
class WINDOWS_1255Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var WINDOWS_1255
	 */
	protected WINDOWS_1255 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\WINDOWS_1255', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('windows-1255', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('windows-1255', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2255', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Microsoft  [http://www.iana.org/assignments/charset-reg/windows-1255] [Katya_Lazhintseva]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/windows-1255] [Katya_Lazhintseva', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'cswindows1255',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new WINDOWS_1255();
	}
	
}
