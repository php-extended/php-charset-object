<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\KSC5636;
use PHPUnit\Framework\TestCase;

/**
 * KSC5636Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\KSC5636
 *
 * @internal
 *
 * @small
 */
class KSC5636Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var KSC5636
	 */
	protected KSC5636 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\KSC5636', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('KSC5636', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('KSC5636', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('102', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertNull($this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'ISO646-KR',
			'csKSC5636',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new KSC5636();
	}
	
}
