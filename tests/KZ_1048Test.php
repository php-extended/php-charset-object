<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\KZ_1048;
use PHPUnit\Framework\TestCase;

/**
 * KZ_1048Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\KZ_1048
 *
 * @internal
 *
 * @small
 */
class KZ_1048Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var KZ_1048
	 */
	protected KZ_1048 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\KZ_1048', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('KZ-1048', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('KZ-1048', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('119', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.iana.org/assignments/charset-reg/KZ-1048]      [Sairan_M_Kikkarin][Alexei_Veremeev]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.iana.org/assignments/charset-reg/KZ-1048]      [Sairan_M_Kikkarin][Alexei_Veremeev', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'STRK1048-2002',
			'RK1048',
			'csKZ1048',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new KZ_1048();
	}
	
}
