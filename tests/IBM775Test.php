<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM775;
use PHPUnit\Framework\TestCase;

/**
 * IBM775Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM775
 *
 * @internal
 *
 * @small
 */
class IBM775Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM775
	 */
	protected IBM775 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM775', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM775', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM775', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2087', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('HP PCL 5 Comparison Guide (P/N 5021-0329) pp B-13, 1996', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'cp775',
			'csPC775Baltic',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM775();
	}
	
}
