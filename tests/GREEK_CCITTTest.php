<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\GREEK_CCITT;
use PHPUnit\Framework\TestCase;

/**
 * GREEK_CCITTTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\GREEK_CCITT
 *
 * @internal
 *
 * @small
 */
class GREEK_CCITTTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GREEK_CCITT
	 */
	protected GREEK_CCITT $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\GREEK_CCITT', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('greek-ccitt', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('greek-ccitt', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('91', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-150',
			'csISO150',
			'csISO150GreekCCITT',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GREEK_CCITT();
	}
	
}
