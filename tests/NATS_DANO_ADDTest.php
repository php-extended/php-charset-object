<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\NATS_DANO_ADD;
use PHPUnit\Framework\TestCase;

/**
 * NATS_DANO_ADDTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\NATS_DANO_ADD
 *
 * @internal
 *
 * @small
 */
class NATS_DANO_ADDTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NATS_DANO_ADD
	 */
	protected NATS_DANO_ADD $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\NATS_DANO_ADD', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('NATS-DANO-ADD', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('NATS-DANO-ADD', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('34', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-9-2',
			'csNATSDANOADD',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NATS_DANO_ADD();
	}
	
}
