<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\HP_LEGAL;
use PHPUnit\Framework\TestCase;

/**
 * HP_LEGALTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\HP_LEGAL
 *
 * @internal
 *
 * @small
 */
class HP_LEGALTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HP_LEGAL
	 */
	protected HP_LEGAL $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\HP_LEGAL', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('HP-Legal', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('HP-Legal', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2017', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('PCL 5 Comparison Guide, Hewlett-Packard,
HP part number 5961-0510, October 1992
PCL Symbol Set id: 1U', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csHPLegal',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HP_LEGAL();
	}
	
}
