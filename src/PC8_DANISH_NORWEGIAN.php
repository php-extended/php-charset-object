<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Charset;

/**
 * PC8_DANISH_NORWEGIAN class file.
 * 
 * This class contains the metadata about the PC8-Danish-Norwegian character set.
 * 
 * /!\ Do not edit this file by hand, as it is written automatically. /!\
 * See PhpExtended\Charset\CharacterSetGenerator for more details.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortClassName")
 */
class PC8_DANISH_NORWEGIAN implements CharacterSetInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return __CLASS__;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getName()
	 */
	public function getName() : string
	{
		return 'PC8-Danish-Norwegian';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getFullName()
	 */
	public function getFullName() : string
	{
		return 'PC8-Danish-Norwegian';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getMIBenum()
	 */
	public function getMIBenum() : int
	{
		return 2012;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getSourceComment()
	 */
	public function getSourceComment() : ?string
	{
		return 'PC Danish Norwegian
8-bit PC set for Danish Norwegian
PCL Symbol Set id: 11U';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getSourceUrl()
	 */
	public function getSourceUrl() : ?string
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getRFCNumber()
	 */
	public function getRFCNumber() : ?int
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Charset\CharacterSetInterface::getAliases()
	 */
	public function getAliases() : array
	{
		return [
			'csPC8DanishNorwegian',
		];
	}
	
}
