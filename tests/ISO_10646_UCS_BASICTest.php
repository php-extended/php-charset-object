<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_10646_UCS_BASIC;
use PHPUnit\Framework\TestCase;

/**
 * ISO_10646_UCS_BASICTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_10646_UCS_BASIC
 *
 * @internal
 *
 * @small
 */
class ISO_10646_UCS_BASICTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_10646_UCS_BASIC
	 */
	protected ISO_10646_UCS_BASIC $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_10646_UCS_BASIC', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-10646-UCS-Basic', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-10646-UCS-Basic', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('1002', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('ASCII subset of Unicode.  Basic Latin = collection 1
See ISO 10646, Appendix A', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUnicodeASCII',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_10646_UCS_BASIC();
	}
	
}
