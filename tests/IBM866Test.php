<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM866;
use PHPUnit\Framework\TestCase;

/**
 * IBM866Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM866
 *
 * @internal
 *
 * @small
 */
class IBM866Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM866
	 */
	protected IBM866 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM866', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM866', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM866', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2086', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM NLDG Volume 2 (SE09-8002-03) August 1994', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'cp866',
			'866',
			'csIBM866',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM866();
	}
	
}
