<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\AMIGA_1251;
use PHPUnit\Framework\TestCase;

/**
 * AMIGA_1251Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\AMIGA_1251
 *
 * @internal
 *
 * @small
 */
class AMIGA_1251Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AMIGA_1251
	 */
	protected AMIGA_1251 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\AMIGA_1251', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Amiga-1251', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('Amiga-1251', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2104', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('See [http://www.amiga.ultranet.ru/Amiga-1251.html]', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertEquals('http://www.amiga.ultranet.ru/Amiga-1251.html', $this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'Ami1251',
			'Amiga1251',
			'Ami-1251',
			'csAmiga1251',
			'(Aliases are provided for historical reasons and should not be used) [Malyshev]',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AMIGA_1251();
	}
	
}
