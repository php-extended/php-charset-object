<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\JUS_I_B1_003_SERB;
use PHPUnit\Framework\TestCase;

/**
 * JUS_I_B1_003_SERBTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\JUS_I_B1_003_SERB
 *
 * @internal
 *
 * @small
 */
class JUS_I_B1_003_SERBTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JUS_I_B1_003_SERB
	 */
	protected JUS_I_B1_003_SERB $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\JUS_I_B1_003_SERB', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('JUS_I.B1.003-serb', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('JUS_I.B1.003-serb', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('89', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('[ISO-IR: International Register of Escape Sequences]
        Note: The current registration authority is IPSJ/ITSCJ, Japan.', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'iso-ir-146',
			'serbian',
			'csISO146Serbian',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JUS_I_B1_003_SERB();
	}
	
}
