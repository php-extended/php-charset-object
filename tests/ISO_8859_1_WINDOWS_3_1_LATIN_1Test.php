<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\ISO_8859_1_WINDOWS_3_1_LATIN_1;
use PHPUnit\Framework\TestCase;

/**
 * ISO_8859_1_WINDOWS_3_1_LATIN_1Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\ISO_8859_1_WINDOWS_3_1_LATIN_1
 *
 * @internal
 *
 * @small
 */
class ISO_8859_1_WINDOWS_3_1_LATIN_1Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ISO_8859_1_WINDOWS_3_1_LATIN_1
	 */
	protected ISO_8859_1_WINDOWS_3_1_LATIN_1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\ISO_8859_1_WINDOWS_3_1_LATIN_1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('ISO-8859-1-Windows-3.1-Latin-1', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('ISO-8859-1-Windows-3.1-Latin-1', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2001', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('Extended ISO 8859-1 Latin-1 for Windows 3.1.
PCL Symbol Set id: 19U', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertNull($this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csWindows31Latin1',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ISO_8859_1_WINDOWS_3_1_LATIN_1();
	}
	
}
