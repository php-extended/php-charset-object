<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\IBM863;
use PHPUnit\Framework\TestCase;

/**
 * IBM863Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\IBM863
 *
 * @internal
 *
 * @small
 */
class IBM863Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IBM863
	 */
	protected IBM863 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\IBM863', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('IBM863', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('IBM863', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('2050', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertEquals('IBM Keyboard layouts and code pages, PN 07G4586 June 1991', $this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'cp863',
			'863',
			'csIBM863',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IBM863();
	}
	
}
