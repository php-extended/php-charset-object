<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\US_DK;
use PHPUnit\Framework\TestCase;

/**
 * US_DKTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\US_DK
 *
 * @internal
 *
 * @small
 */
class US_DKTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var US_DK
	 */
	protected US_DK $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PhpExtended\\Charset\\US_DK', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('us-dk', $this->_object->getName());
	}
	
	public function testGetFullName() : void
	{
		$this->assertEquals('us-dk', $this->_object->getFullName());
	}
	
	public function testGetMIBenum() : void
	{
		$this->assertEquals('100', $this->_object->getMIBenum());
	}
	
	public function testGetSourceComment() : void
	{
		$this->assertNull($this->_object->getSourceComment());
	}
	
	public function testGetSourceUrl() : void
	{
		$this->assertNull($this->_object->getSourceUrl());
	}
	
	public function testGetRFCNumber() : void
	{
		$this->assertEquals(1345, $this->_object->getRFCNumber());
	}
	
	public function testGetAliases() : void
	{
		$this->assertEquals([
			'csUSDK',
		], $this->_object->getAliases());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new US_DK();
	}
	
}
